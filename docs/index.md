# Homepage

Welcome! 

This website features three workshops as an extension of the main GitLab workshops found at [GitLab Learn Labs](https://gitlab.com/gitlab-learn-labs).

Featured workshops:

- [GitLab AI-Powered DevSecOps Platform Evaluation (external)](https://ai-devsecops-guided-evaluation-gl-demo-ultimate--70925c7a805a76.gitlab.io/)
- [Security & Compliance with GitLab](sec_intro.md)
- [Intro to Agile ProjectManagement  with GitLab](pm_intro.md)
- [Implementation Planning with GitLab](imp_intro.md)
- [Onboarding with GitLab](on_intro.md)

If you have any questions, comments, or feedback, please feel free to reach out to me at [smathur@gitlab.com](mailto:smathur@gitlab.com).
