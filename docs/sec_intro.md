# Security & Compliance with GitLab

In this walkthrough, you will be guided through how to set up application security including security scanners, scan enforcement policies, and how to interpret the data from these scans. 

## Requirements

**To enable security scanning in your project, you will need the following:**
    - A GitLab project that meets the requirements of the security scan you choose to enable, with CI enabled
    - A .gitlab-ci.yml file for the project that has at least a build job defined
    - A Linux-based GitLab Runner with the Docker or Kubernetes executor
